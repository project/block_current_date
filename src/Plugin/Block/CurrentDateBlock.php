<?php

/**
 * @file
 * Contains \Drupal\block_current_date\Plugin\Block\DonBlock.
 */

namespace Drupal\block_current_date\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a current date block.
 *
 * @Block(
 *   id = "block_current_date",
 *   admin_label = @Translation("Block Current Date"),
 *   category = @Translation("Custom block current date")
 * )
 */
class CurrentDateBlock extends BlockBase implements BlockPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    $default_config = \Drupal::config('block_current_date.settings');
    return [
      'format' => $default_config->get('format'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $config = $this->getConfiguration();
    $current_time = \Drupal::time()->getCurrentTime();
    $date= date($config['format'], $current_time);


    return [
      '#theme' => 'block_current_date',
      '#currentDate' => $date,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['format'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#title' => $this->t('Format'),
      '#description' => $this->t('Format date: Example l, F j, Y - H:i'),
      '#default_value' => isset($config['format']) ? $config['format'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['format'] = $values['format'];
  }


}

